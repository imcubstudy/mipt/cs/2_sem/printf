; nasm -fmacho64 stdprintf.asm -l stdprintf.lst
; ld stdprintf.o -lc -o stdprintf -arch x86_64 -macosx_version_min 10.13

_UNIX_P_	equ		0x2000000

extern 		_printf

section		.text

global	_main
_main:
	push	rbp
	mov		rbp, rsp
	
	mov		rdi, f_str				; printf("Hello, World\n")
	mov		rsi, 0xEDA
	xor		rax, rax
	call	_printf
	
	pop		rbp
	
	xor		rax, rax
	ret

section		.data

f_str:		db		`Hello, %x\n\0`

