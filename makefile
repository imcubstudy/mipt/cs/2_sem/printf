LD	:= ld
CC	:= clang
AC	:= nasm
RM	:= rm

stdprintf:
	$(AC) -fmacho64 stdprintf.asm -l stdprintf.lst
	$(LD) stdprintf.o -lc -o stdprintf -arch x86_64 -macosx_version_min 10.10
	./stdprintf

asmprintf:
	$(AC) -fmacho64 printf.asm -l printf.lst
	$(CC) test.cpp -c
	$(CC) -Wl,-no_pie printf.o test.o -o exec
	./exec

clean:
	$(RM) exec stdprintf *.o

