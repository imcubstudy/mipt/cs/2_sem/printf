; nasm -fmacho64 printf.asm -l printf.lst
; clang -Wl,-no_pie printf.o test.o -o exec

_UNIX_P_	equ		0x2000000		; prefix for unix syscalls

section		.text

%macro _$pass_arg 0
	
	inc		rsi
	mov		rdi, rsi

	add		r15, 08h

%endmacro

%macro _$count_sys 1

		push	rax				; save rax and rsi
		push	rsi

		mov		rdi, [rbp + r15]
		mov		rsi, num_buf	; create buf
		call	make_%1
		
		mov		rbx, rax

		mov		rdx, rax		; print it
		mov		rdi, 01h
		mov		rax, _UNIX_P_ + 04d
		syscall
			
		pop		rsi				; revive rax and rsi
		pop		rax

		add		rax, rbx
		xor		rbx, rbx

		_$pass_arg

%endmacro          

%macro _$COSTYL 0 

	push	r9
	push	r8
	push	rcx
	push	rdx
	push	rsi

%endmacro

;---------------------------------------------------------------------
;					printf
;
;	argument list:
;		rdi		= format string terminated by `\0`
;		stack	- args for print
;
;	$	format string specificators:
;	$		u_int64:
;	$			%d	- decimal
;	$			%o  - octal
;	$			%x	- hex
;	$			%b	- bin
;	$
;	$		other:
;	$			%s	- char string ( terminated by `\0` ) by address
;	$			%c	- char ( u_int64 )
;
;	return list:
;		rax		= char printed
;
;	destr list:
;		rbx, rcx, rsi, rdx, r8, r15, r9, r11
;---------------------------------------------------------------------
global _printf

_printf:					;		VERY COSTYL
	pop		r14				;		
							;		for fastcall 
	_$COSTYL 				;		compatibility
							;
	call	printf			;
							;
	add		rsp, 5d * 8d	;
							;
	push	r14				;
	ret						;
	
printf:
	push	rbp
	mov		rbp, rsp

	mov		r15, 10h

	xor		rax, rax
	xor		rbx, rbx

	mov		rsi, rdi
	.loop:
		cmp		[rsi], byte 00h
		je		.loop_end
	
		cmp		[rsi], byte '%'
		je		.spec
		
		inc		rsi
		inc		rbx

		jmp		.loop
		.spec:
			push	rax						; save rax and rsi
			push	rsi

			mov		rax, _UNIX_P_ + 04d		; drop buffer
			mov		rsi, rdi
			mov		rdi, 01d
			mov		rdx, rbx
			syscall

			pop		rsi						; revive rax and rsi
			pop		rax

			add		rax, rbx
			xor		rbx, rbx

			inc		rsi
			mov		rdi, rsi

			cmp		[rsi], byte	'd'			; specificator switch
			je		.dec
			cmp		[rsi], byte 's'
			je		.string
			cmp		[rsi], byte 'x'
			je		.hex
			cmp		[rsi], byte 'b'
			je		.bin
			cmp		[rsi], byte 'o'
			je		.oct
			cmp		[rsi], byte 'c'
			je		.char
			cmp		[rsi], byte '%'
			je		.percent
		
			.string:
				push	rax				; save rax and rsi
				push	rsi

				mov		rdi, [rbp + r15]
				call	strlen

				mov		rbx, rax

				mov		rdx, rax
				mov		rsi, [rbp + r15]
				mov		rax, _UNIX_P_ + 04d
				mov		rdi, 01d
				syscall

				pop		rsi
				pop		rax
				
				add		rax, rbx
				xor		rbx, rbx

				_$pass_arg
				
				jmp		.loop
			
			.char:
				push	rsi
				push	rax

				mov		rax, _UNIX_P_ + 04d
				mov		rdi, 01h
				mov		rsi, num_buf
				mov		rdx, [rbp + r15]
				mov		byte [rsi], dl
				mov		rdx, 01h
				syscall

				pop		rax
				pop		rsi

				inc		rax
			
				_$pass_arg

				jmp		.loop

			.percent:
				push	rsi
				push	rax

				mov		rax, _UNIX_P_ + 04d
				mov		rdi, 01h
				mov		rsi, num_buf
				mov		byte [rsi], '%'
				mov		rdx, 01h
				syscall

				pop		rax
				pop		rsi

				inc		rax

				inc		rsi
				mov		rdi, rsi

				jmp		.loop

			%macro _$count_sys_call 1
				.%1:
					_$count_sys %1
					jmp		.loop
			%endmacro
			
			_$count_sys_call dec
			_$count_sys_call hex
			_$count_sys_call oct
			_$count_sys_call bin

			jmp		.loop	
			
	.loop_end:

	add		rax, rbx
	push	rax

	mov		rax, _UNIX_P_ + 04d
	mov		rsi, rdi
	mov		rdi, 01d
	mov		rdx, rbx
	syscall

	pop		rax
	pop		rbp

	ret

;--------------------------------------------------------
;					strlen
;
;	argument list:
;		rdi		= string terminated by `\0`
;	
;	return list:
;		rax		= len string
;
;	destr list:
;		rcx, rdi
;		cld
;--------------------------------------------------------
strlen:
	cld

	mov		rcx, -1h
	xor		rax, rax
	repnz	scasb

	mov		rax, rcx
	neg		rax
	sub		rax, 2

	ret

;#########################################################
section 	.data

num_buf:	times 	64 db '@'
.len		equ		$ - num_buf

cs_chars:	db 		`0123456789ABCDEF`
.len		equ 	$ - cs_chars

section		.text
;########################################################

;--------------------------------------------------------
;					make_%1
;	   bin, oct, hex, dec, or *{ n where n is natural }*
;	argument list:
;		rdi		= u_int64 num
;		rsi		= char buf for result
;	
;	return list:
;		rax		= len of result string
;		rsi		= result string
;
;	destr list:
;		rcx, rdx, r8, rdi, r9
;--------------------------------------------------------
%macro _$pass_cs_char 0

	sub		rdx, num_buf.len
	mov		dl,  byte [rdx + r9]
	mov		[rsi], dl 

%endmacro

%macro _$make_rand_cs_str 2

make_%1:
	mov		rax, rdi
	mov		rcx, %2d

	mov		r9,  cs_chars

	xor		r8, r8

	add		rsi, num_buf.len
	.loop:
		dec		rsi
		inc		r8

		xor		rdx, rdx
		div		rcx

		_$pass_cs_char
	
		cmp		rax, 00h
		ja		.loop

	mov		rax, r8
	ret

%endmacro

%macro	_$make_pow2_cs_str 3

make_%1:
	xor		rax, rax
	add		rsi, num_buf.len

	mov		r9,  cs_chars

	mov		rcx, %2b

	.loop:
		dec		rsi

		mov		rdx, rdi
		and		rdx, rcx

		_$pass_cs_char
		
		shr		rdi, %3h
		inc		rax

		cmp		rdi, 00h
		jne		.loop

	ret

%endmacro

_$make_rand_cs_str dec, 10

_$make_pow2_cs_str bin, 1,    1
_$make_pow2_cs_str oct, 111,  3
_$make_pow2_cs_str hex, 1111, 4

