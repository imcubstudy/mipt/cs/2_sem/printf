// clang -c test.cpp

#include <cstdint>

extern "C"
uint64_t printf( const char*, ... );

int main()
{
	uint64_t num = 0xDED;
	uint64_t c   = '!';
	uint64_t zer = 0;

	printf(  "\nasmprintf 1.0\nstring\t\t%s\nChar\t\t%c\nBin\t\t%b\nHex\t\t%x\nOct\t\t%o\nDec\t\t%d\n", 
			 "User\'s string", c, zer, zer, zer, zer );
	
	printf(  "\nasmprintf 1.0\nstring\t\t%s\nChar\t\t%c\nBin\t\t%b\nHex\t\t%x\nOct\t\t%o\nDec\t\t%d\n", 
			 "User\'s string", c, num, num, num, num );
	
	printf( "%d chars\n", 
		printf( "\n0123456789, end %c %s %x %d %%\n", 
			(uint64_t)'I', 
			"Love", 
			(uint64_t)3802, 
			(uint64_t)100 ) );

	return 0;
}

